elixir (0.7.1-5) UNRELEASED; urgency=medium

  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Tue, 13 Feb 2018 10:16:57 +0100

elixir (0.7.1-4) unstable; urgency=high

  * Team upload.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Piotr Ożarowski ]
  * Apply fix for CVE-2012-2146 from RedHat's bugzilla (closes: 670919)
    (https://bugzilla.redhat.com/show_bug.cgi?id=CVE-2012-2146)

 -- Ondřej Nový <novy@ondrej.org>  Tue, 29 Mar 2016 21:29:24 +0200

elixir (0.7.1-3) unstable; urgency=medium

  * Team upload.

  [ Piotr Ożarowski ]
  * Remove myself from Uploaders

  [ Scott Kitterman ]
  * Rebuild for sqlalchemy 1.0
  * Update debian/watch to use pypi.debian.net redirector
  * Add python-crypto to build-depends for test execution

 -- Scott Kitterman <scott@kitterman.com>  Sat, 01 Aug 2015 21:34:44 -0400

elixir (0.7.1-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Piotr Ożarowski ]
  * Add sa_0.9_compatibility.patch (closes: 739476)
  * Convert package to dh_python2 and pybuild buildsystem
  * Remove debian/preinst and debian/pycompat files, no longer needed
  * Change debhelper compatibility level to 9
  * Source format changed to 3.0 (quilt)
  * Bump Standards-Version to 3.9.5 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 02 Mar 2014 00:39:08 +0100

elixir (0.7.1-1) unstable; urgency=low

  * New upstream release
    - examples are no longer in the tarball
  * Convert package to dh sequencer
  * Bump Standards-Version to 3.8.4 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 28 Jan 2010 19:58:27 +0100

elixir (0.7.0-1) unstable; urgency=low

  * New upstream release
  * Convert to python-support
    - add preinst file to remove old .pyc files
  * Bump Standards-Version to 3.8.3 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 01 Oct 2009 20:49:56 +0200

elixir (0.6.1-2) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Piotr Ożarowski ]
  * Add ${misc:Depends} to Depends
  * Change Debian packaging license to MIT (to match upstream)
  * Upoad to unstable

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 22 Feb 2009 22:37:23 +0100

elixir (0.6.1-1) experimental; urgency=low

  * New upstream release
    (upload to experimental due to Lenny freeze, python-turbogears
     recommends this package)

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 19 Aug 2008 00:34:37 +0200

elixir (0.6.0-1) unstable; urgency=medium

  * New upstream release
  * python-sqlalchemy's required version bumped to 0.4.0
  * Bump Standards-Version to 3.8.0 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 20 Jul 2008 23:34:01 +0200

elixir (0.5.2-1) unstable; urgency=low

  * New upstream release
  * debian/watch file updated (s/cheeseshop/pypi)

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 28 Mar 2008 22:30:26 +0100

elixir (0.5.1-2) unstable; urgency=medium

  * No need to rename Egg dir name anymore as pycentral handles it now
    (Closes: #472036)
  * Move python-central to Build-Depends-Indep
  * Bumped python-central required version to 0.6 (new .py files location)
  * Strip the "-1" from setuptools' required build version (to ease backports)

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 21 Mar 2008 21:10:19 +0100

elixir (0.5.1-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - fix Vcs-Browser field

  [ Piotr Ożarowski ]
  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 07 Feb 2008 20:15:12 +0100

elixir (0.5.0-1) unstable; urgency=low

  * New upstream release
  * Remove dont_install_example_files_in_site-packages patch (applied upstream)
  * Add python-crypto to Recommends (elixir.ext.encrypted plugin)
  * Bump Standards-Version to 3.7.3 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 16 Dec 2007 13:31:17 +0100

elixir (0.4.0-1) unstable; urgency=low

  * New upstream release
  * Add dont_install_example_files_in_site-packages patch
  * Bump python-sqlalchemy required version to 0.3.9
  * Homepage field added
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)
  * Remove useless Provides field

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 29 Oct 2007 21:09:13 +0100

elixir (0.3.0-1) unstable; urgency=low

  * New upstream release
  * Changed my address to piotr@debian.org

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 10 Apr 2007 15:52:41 +0200

elixir (0.2.0-1) unstable; urgency=low

  * Initial release. (Closes: #412218)

 -- Piotr Ozarowski <ozarow@gmail.com>  Wed, 28 Feb 2007 15:07:30 +0100
